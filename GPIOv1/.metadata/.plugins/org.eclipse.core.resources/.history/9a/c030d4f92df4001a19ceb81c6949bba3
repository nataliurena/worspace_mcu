/*
===============================================================================
 Name        : gpio.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "string.h"

#include "Rfid.h"

#define		PINMODE_PULLUP 			0
#define		PINMODE_REPEAT			1
#define		PINMODE_HIGH_IMP 		2
#define		PINMODE_PULLDOWN 		3


#define MAX_LEN 16
#define MAX_ID 6

xSemaphoreHandle xSemaforo; // debe ser global para que sea visto por todas las tareas


#define CANT_CLIENTES 3 //cantidad de clientes
volatile uint64_t registros[CANT_CLIENTES] = {0x35da7889, 0xeb28d773, 0x36d57889};

void setup_MFRC522(void);
int Card(char *pcard);
uint16_t isCardRegistered (uint8_t checksum);
void Write_MFRC522(uint8_t addr, uint8_t val);
uint8_t Read_MFRC522(uint8_t addr);
uint8_t MFRC522_Request(uint8_t reqMode, uint8_t *TagType);
uint8_t MFRC522_Anticoll(uint8_t *serNum);
uint8_t MFRC522_ToCard(uint8_t command, uint8_t *sendData, uint8_t sendLen, uint8_t *backData, uint32_t *backLen);
uint8_t MSS_SPI_transfer_frame(uint32_t tx_bits);


void MFRC522_Init(void);
void AntennaOn(void);

void SPI_init(void);


//!< ///////////////////   PCONP   //////////////////////////
	//!<  Power Control for Peripherals register (PCONP - 0x400F C0C4) [pag. 62 user manual LPC1769]
	//!< 0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
	#define 	PCONP	(* ( ( __RW uint32_t  * ) 0x400FC0C4UL ))


//!< ///////////////////   PCLKSEL   //////////////////////////
	//!< Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 -0x400F C1A8 and PCLKSEL1 - 0x400F C1AC) [pag. 56 user manual]
	//!< 0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los CLKs de los dispositivos:
	#define		PCLKSEL		( ( __RW uint32_t  * ) 0x400FC1A8UL )
	//!< Registros PCLKSEL
	#define		PCLKSEL0	PCLKSEL[0]
	#define		PCLKSEL1	PCLKSEL[1]



//------------------MFRC522 Register---------------
//Page 0:Command and Status
/*#define     Reserved00            0x00

*/
/*
//Page 1:Command
*/
/*
// Mifare_One card command word
# define PICC_REQIDL          0x26               // find the antenna area does not enter hibernation
# define PICC_REQALL          0x52               // find all the cards antenna area
# define PICC_ANTICOLL        0x93               // anti-collision
# define PICC_SElECTTAG       0x93               // election card
# define PICC_AUTHENT1A       0x60               // authentication key A
# define PICC_AUTHENT1B       0x61               // authentication key B
# define PICC_READ            0x30               // Read Block
# define PICC_WRITE           0xA0               // write block
# define PICC_DECREMENT       0xC0               // debit
# define PICC_INCREMENT       0xC1               // recharge
# define PICC_RESTORE         0xC2               // transfer block data to the buffer
# define PICC_TRANSFER        0xB0               // save the data in the buffer
# define PICC_HALT            0x50               // Sleep




//MF522 Command word
#define PCD_IDLE              0x00               //NO action; Cancel the current command
#define PCD_AUTHENT           0x0E               //Authentication Key
#define PCD_RECEIVE           0x08               //Receive Data
#define PCD_TRANSMIT          0x04               //Transmit data
#define PCD_TRANSCEIVE        0x0C               //Transmit and receive data,
#define PCD_RESETPHASE        0x0F               //Reset
#define PCD_CALCCRC           0x03               //CRC Calculate



#define     __R				volatile const  	// !< Modificador para solo lectura
#define 	__W     		volatile 	       	// !<  Modificador para solo escritura
#define 	__RW			volatile           	// !< Modificador lectura / escritura
*/

typedef struct{//MAPA DE REGISTROS DE SPI

	union{
		__W uint32_t S0SPCR; //SPI CONTROL REGISTER (setea SPI0 con los siguientes parametros):
		struct{
			__RW uint32_t RESERVED_1:2;
			__RW uint32_t BitEnable:1;//0: send and receives 8 bits (DATALOW); 1: amplia BUFFER sumandole BITS(BITS=nombre de registro)
			__RW uint32_t CPHA:1;//0:muestra en primer flanco de SCK. Empieza cuando se activa SSEL signal; 1:
			__RW uint32_t CPOL:1;//Polarizacion de pulso SCK(tiempo). 0:SCK active high; 1:SCK active low
			__RW uint32_t MSTR:1;//Seleccion de Modo. 0: slave mode; 1:Master mode
			__RW uint32_t LSBF:1;//Sea BUFFER byte de transferencia. 0:MSB(lee BUFFER de izquierda a derecha); 1:LSB (derecha a izquierda)
			__RW uint32_t SPIE:1;//Habilitacion de INTERRUPCION. 0:interrupt inhibited; 1: interrupcion generada cada vez que SPIF o MODF se activen
			__RW uint32_t BITS:4;//si BitEnable=1 -> BITS=number of bits per transfer (Ej: 1000 = 8 bits per transfer. Ej2: 1100 = 12 bits per transfer. Ej3: 0000 = 16 bits per transfer)
			__RW uint32_t RESERVED_2:20;
		};
	};

	union{
		__RW uint32_t S0SPSR; //SPI STATUS REGISTER (los bits de este registro se BORRAN/LIMPIAN al ser leidos):
		struct{
			__RW uint32_t RESERVED_3:3;
			__RW uint32_t ABRT:1; //Se pone en 1 si el Slave se desconecta.
			__RW uint32_t MODF:1;//Se pone en 1 indicando falla de Modo(error set control).
			__RW uint32_t ROVR:1;//Se pone en 1 cuando read overrun (BUFFER lectura lleno)
			__RW uint32_t WCOL:1;//Se pone en 1 cuando write collision (BUFFER salida lleno)
			__RW uint32_t SPIF:1;//TRANSFER COMPLETE (flag) -> se pone 1. Ocurre en el ultimo ciclo de transferencia
			__RW uint32_t RESERVED_4:24;
		};
	};

	union{
		__RW uint32_t S0SPDR;//SPI BI-DIRECTIONAL DATA REGISTER(entradas y salidas):
//NOTAS DE DATA REGISTER: cuando se usa como MASTER, escribiendo en este registro se empieza el SPI data transfer.
//		cuando se empieza la transferencia se bloquea este buffer y no se puede escribir. Lo mismo ocurre con SPIF=1 (leyendo SPIF se limpia)
		struct{
			__RW uint32_t BUFFER:8;//puerto de SPI bi-directional
			__RW uint32_t DataHigh:8;//si BitEnable=1 -> se habilita este espacio para ampliar el BUFFER sumandole BITS(nombre de registro)
			__RW uint32_t RESERVED_5:16;
			};
		};

	union{
		__RW uint32_t S0SPCCR;//CONTROLS FREQUENCY OF MASTER's SCK0(frecuencia de pulsos de tiempo):
		struct{
			__RW uint32_t CLOCK:8;//setea SCK(numero de pulsos por ciclo). Si es Master-> T>=8. Si es Slave-> T< 1/8 de SPI of Master
			__RW uint32_t RESERVED_6:24;
			};
		};

/*	union{
		__RW uint32_t S0SPINT;//SPI INTERRUPT FLAG
		struct{
			__RW uint32_t SPINT:1;//se borra con un 1
		};
		__RW uint32_t RESERVED_7:31;
	};
*/
}spi_t;


#define SPI ((__RW spi_t *) 0x40020000UL)



//And MF522 The error code is returned when communication
#define MI_OK                 0
#define MI_NOTAGERR           1
#define MI_ERR                2



#define SCK_CLCK 0b01	//0b00= CCLK/4		; 	0b01= CCLK	; 	0b10= CCLCK/2	;	 0b11= CCLCK/8
#define SPI_MODE 1//Modo que va a tomar el micro: 0: Slave ; 	1: Master
#define SPI_FUNCTION 3


//----------PINES----------------------------------------------------------------------------------------
#define SCK_PIN		0, 15 //alternativa en    0, 7  1, 20
#define SSEL_PIN	0, 16//alternativa en     0, 6   1, 21
#define MISO_PIN	 0, 17//alternativa en    0, 8  1, 23
#define MOSI_PIN	0, 18 //alternativa en	  0, 9   1, 24



#define LOW		0 //bajo
#define HIGH	1 //alto




//#define SPI_ChipSelect_LOW()	SetPin( SSEL_PIN, LOW )//antes estaba en 0
//#define SPI_ChipSelect_HIGH()	SetPin( SSEL_PIN, HIGH )
#define SPI_ChipSelect_LOW()	 Chip_GPIO_SetPinOutLow(LPC_GPIO,SSEL_PIN)
#define SPI_ChipSelect_HIGH()	 Chip_GPIO_SetPinOutHigh(LPC_GPIO, SSEL_PIN)



//FUNCION Card: primitiva de MFRC522 para acceso por tarjeta magnetica
// return: hay tarjeta valida o no
int Card(char *pcard)
{
	uint8_t status, checksum1, str[MAX_LEN];
	int8_t isCard;
	uint32_t i;

	//Initialize variables
	checksum1 = 0;
	isCard = 1;
	pcard[0] = 0x0;

	    // Find cards
	    status = MFRC522_Request(PICC_REQIDL, str);

	    // Anti-collision, return: serial number == 4 bytes
	    status = MFRC522_Anticoll(str);
	    if (status == MI_OK)
	    {
			checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3];
			pcard[0] = str[0];
			pcard[1] = str[1];
			pcard[2] = str[2];
			pcard[3] = str[3];
			pcard[4] = str[4];
	    }

	    //con el if se fija que haya tarjeta
	    if( 0x10 == checksum1 || 0x00 == checksum1)	//si no hay tarjeta
	    	isCard = 0;
	    //NOTA: el registro donde el MFRC522 guarda los ID se encuentra con contenido (distinto de 0 porque lo usa el modulo para inicializar)
	    // |-> cuando NO hay tarjeta se lee ese contenido y se guarda en buffer STR con SPI
	    // |-> cuando hay tarjeta el MFRC522 cola la ID en ese registro, despues se lee y se guarda en buffer STR con SPI

	    //CONCLUSION: si no hay tarjeta, se lee contenido que no es ID y se chequea con el CHECKSUM1

	    MFRC522_Halt(); //modo hibernacion
	  return isCard ;
}


//FUNCION MFRC522_Request: find cards
// argumento: ReqMode = find cards way
// return: successful or no
uint8_t MFRC522_Request(uint8_t reqMode, uint8_t *TagType)
{
  uint8_t status;
  uint32_t backBits; // The received data bits

  Write_MFRC522(BitFramingReg, 0x07);   // TxLastBists = BitFramingReg[2..0]

  TagType[0] = reqMode;

  status = MFRC522_ToCard(PCD_TRANSCEIVE, TagType, 1, TagType, &backBits);
  if ((status != MI_OK) || (backBits != 0x10)) {
    status = MI_ERR;
  }

  return status;
}




/*
 * Function Name: MFRC522_Anticoll
 * Description: Anti-collision detection, reading selected card serial number card
 * Input parameters: serNum - returns 4 bytes card serial number, the first 5 bytes for the checksum byte
 * Return value: the successful return MI_OK
 */
uint8_t MFRC522_Anticoll(uint8_t *serNum)
{
  uint8_t status;
  uint8_t i;
  uint8_t serNumCheck=0;
  uint32_t unLen;

  Write_MFRC522(BitFramingReg, 0x00);		//TxLastBists = BitFramingReg[2..0]

  serNum[0] = PICC_ANTICOLL;
  serNum[1] = 0x20;
  status = MFRC522_ToCard(PCD_TRANSCEIVE, serNum, 2, serNum, &unLen);

  if (status == MI_OK)
  {
    //Check card serial number
    for (i=0; i<4; i++)
    {
      serNumCheck ^= serNum[i];
    }
    if (serNumCheck != serNum[i])
    {
      status = MI_ERR;
    }
  }

  return status;
}





// FUNCION MFRC522_ToCard: RC522 and ISO14443 (nombre standard para tarjeta magnetica) card communication
// argumento: command = MFRC522 command word
//			 sendData = MFRC522 sent to the card by the data
//			 sendLen = Length of data sent
//			 backData = Received the card returns data,
//			 backLen = Return data bit length
// Return value: the successful return MI_OK
uint8_t MFRC522_ToCard(uint8_t command, uint8_t *sendData, uint8_t sendLen, uint8_t *backData, uint32_t *backLen)
{
  uint8_t status = MI_ERR;
  uint8_t irqEn = 0x00;
  uint8_t waitIRq = 0x00;
  uint8_t lastBits;
  uint8_t n;
  uint32_t i;

  switch (command)
  {
    case PCD_AUTHENT:     // Certification cards close
      {
        irqEn = 0x12;
        waitIRq = 0x10;
        break;
      }
    case PCD_TRANSCEIVE:  // Transmit FIFO data
      {
        irqEn = 0x77;
        waitIRq = 0x30;
        break;
      }
    default:
      break;
  }

  Write_MFRC522(CommIEnReg, irqEn|0x80);  // Interrupt request
  ClearBitMask(CommIrqReg, 0x80);         // Clear all interrupt request bit
  SetBitMask(FIFOLevelReg, 0x80);         // FlushBuffer=1, FIFO Initialization

  Write_MFRC522(CommandReg, PCD_IDLE);    // NO action; Cancel the current command

  // Writing data to the FIFO
  for (i=0; i<sendLen; i++)
  {
    Write_MFRC522(FIFODataReg, sendData[i]);
  }

  // Execute the command
  Write_MFRC522(CommandReg, command);
  if (command == PCD_TRANSCEIVE)
  {
    SetBitMask(BitFramingReg, 0x80);      // StartSend=1,transmission of data starts
  }

  // Waiting to receive data to complete
  i = 2000;	// i according to the clock frequency adjustment, the operator M1 card maximum waiting time 25ms
  do
  {
    // CommIrqReg[7..0]
    // Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
    n = Read_MFRC522(CommIrqReg);
    i--;
  }
  while ((i!=0) && !(n&0x01) && !(n&waitIRq));

  ClearBitMask(BitFramingReg, 0x80);      // StartSend=0

  if (i != 0)
  {
    if(!(Read_MFRC522(ErrorReg) & 0x1B))  // BufferOvfl Collerr CRCErr ProtecolErr
    {
      status = MI_OK;
      if (n & irqEn & 0x01)
      {
        status = MI_NOTAGERR;             // ??
      }

      if (command == PCD_TRANSCEIVE)
      {
        n = Read_MFRC522(FIFOLevelReg);
        lastBits = Read_MFRC522(ControlReg) & 0x07;
        if (lastBits)
        {
          *backLen = (n-1)*8 + lastBits;
        }
        else
        {
          *backLen = n*8;
        }

        if (n == 0)
        {
          n = 1;
        }
        if (n > MAX_LEN)
        {
          n = MAX_LEN;
        }

        // Reading the received data in FIFO
        for (i=0; i<n; i++)
        {
          backData[i] = Read_MFRC522(FIFODataReg);
        }
      }
    }
    else {
      status = MI_ERR;
    }
  }

  return status;
}

/*
 * Function Name: ClearBitMask
 * Description: clear RC522 register bit
 * Input parameters: reg - register address; mask - clear bit value
 * Return value: None
*/
void ClearBitMask(uint8_t reg, uint8_t mask)
{
    uint8_t tmp;
    tmp = Read_MFRC522(reg);
    Write_MFRC522(reg, tmp & (~mask));  // clear bit mask
}



/*
 * Function Name：SetBitMask
 * Description: Set RC522 register bit
 * Input parameters: reg - register address; mask - set value
 * Return value: None
 */
void SetBitMask(uint8_t reg, uint8_t mask)
{
    uint8_t tmp;
    tmp = Read_MFRC522(reg);
    Write_MFRC522(reg, tmp | mask);  // set bit mask
}



//FUNCION MSS_SPI_transfer_frame: utilizada para transferir 2 bytes consecutivos contenido en tx_bits
// argumento: tx_bits =
//					si es para WRITE -> tx_bits buffer de salida
//					si es para READ -> tx_bits debe ser un dummy (ej 0x00)

// CASO SPI_MFRC522: configuracion de MFRC522 por comandos
//
//					si es llamada para WRITE: tx_bits contiene address y dato a escribir en la direccion, recibe un dummy
//					si es llamada para READ: tx_bits contiene en los primeros 8 bits address, resto 0. Recibe dato que se encuentra en address

// return: dummy o dato leido
uint8_t MSS_SPI_transfer_frame(uint32_t tx_bits)
{
    volatile uint32_t dummy;

    /* Flush Rx FIFO. */
    while ( SPI->S0SPSR )
    {
        dummy = SPI->S0SPDR;
        dummy = dummy;  //para no warning de compilador por no usar variable
    }

    /* escribo 2 bytes. */
    SPI->S0SPDR = tx_bits;

    /* espero que se terminen de escribir -> SPIF = 1 */
    while ( SPI->S0SPSR == 0)
    {
        ;
    }

    /* En el mismo tiempo, que se transfiere un dato por el MOSI, llega otro por el MISO  */
    /* Return Rx data */
    return( SPI->S0SPDR );
}



/*
 * Function Name：Read_MFRC522
 * Description: From a certain MFRC522 read a byte of data register
 * Input Parameters: addr - register address
 * Returns: a byte of data read from the
 */
uint8_t Read_MFRC522(uint8_t addr) {
  uint32_t rx_bits;

  // set the select line so we can start transferring
  SPI_ChipSelect_LOW();

  // -Primeros 8 bits: address
  // -ultimos 8 bits: 0x00
  rx_bits = MSS_SPI_transfer_frame(((((addr << 1) & 0x7E) | 0x80) << 8) | 0x00 );

  // clear the select line-- we are done here
  SPI_ChipSelect_HIGH();
	return (uint8_t) rx_bits; // return the rx bits (solo ultimos 8 bits)
}

/*
 * Function Name：Write_MFRC5200
 * Function Description: To a certain MFRC522 register to write a byte of data
 * Input Parameters：addr - register address; val - the value to be written
 * Return value: None
 */
void Write_MFRC522(uint8_t addr, uint8_t val) {
  uint32_t rx_bits;

  // set the select line so we can start transferring
  SPI_ChipSelect_LOW();

  rx_bits = MSS_SPI_transfer_frame( (((addr << 1) & 0x7E) << 8) |  val );
  rx_bits = rx_bits;
  // clear the select line-- we are done here
  SPI_ChipSelect_HIGH();
}



void MFRC522_Halt(void)
{
  uint8_t status;
  uint32_t unLen;
  uint8_t buff[4];

  buff[0] = PICC_HALT;
  buff[1] = 0;
  CalulateCRC(buff, 2, &buff[2]);

  status = MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff,&unLen);
  status = status;
}

/*
 * Function Name: CalulateCRC
 * Description: CRC calculation with MFRC522
 * Input parameters: pIndata - To read the CRC data, len - the data length, pOutData - CRC calculation results
 * Return value: None
 */
void CalulateCRC(uint8_t *pIndata, uint8_t len, uint8_t *pOutData)
{
  uint8_t i, n;

  ClearBitMask(DivIrqReg, 0x04);			//CRCIrq = 0
  SetBitMask(FIFOLevelReg, 0x80);			//Clear the FIFO pointer

  //Writing data to the FIFO
  for (i=0; i<len; i++)
  {
    Write_MFRC522(FIFODataReg, *(pIndata+i));
  }
  Write_MFRC522(CommandReg, PCD_CALCCRC);

  //Wait CRC calculation is complete
  i = 0xFF;
  do
  {
    n = Read_MFRC522(DivIrqReg);
    i--;
  }
  while ((i!=0) && !(n&0x04));			//CRCIrq = 1

  //Read CRC calculation result
  pOutData[0] = Read_MFRC522(CRCResultRegL);
  pOutData[1] = Read_MFRC522(CRCResultRegM);
}






void MFRC522_Reset(void)
{
  Write_MFRC522(CommandReg, PCD_RESETPHASE);
}


/*
 * Function Name：AntennaOn
 * Description: Open antennas, each time you start or shut down the natural barrier between the transmitter should be at least 1ms interval
 * Input: None
 * Return value: None
 */
void AntennaOn(void)
{
  SetBitMask(TxControlReg, 0x03);
}



//INICIALIZACIONES

void MFRC522_Init(void)
{
	SPI_ChipSelect_LOW();
	MFRC522_Reset();

  // Timer: TPrescaler*TreloadVal/6.78MHz = 24ms
  Write_MFRC522(TModeReg, 0x8D);      // Tauto=1; f(Timer) = 6.78MHz/TPreScaler
  Write_MFRC522(TPrescalerReg, 0x3E); // TModeReg[3..0] + TPrescalerReg
  Write_MFRC522(TReloadRegL, 30);
  Write_MFRC522(TReloadRegH, 0);
  Write_MFRC522(TxAutoReg, 0x40);     // force 100% ASK modulation
  Write_MFRC522(ModeReg, 0x3D);       // CRC Initial value 0x6363

  // turn antenna ON
  AntennaOn();
}


void SPI_init(){
		uint8_t limpieza;

		//SetPinsel( SCK_PIN , SPI_FUNCTION );
		Chip_IOCON_PinMuxSet(LPC_IOCON,SCK_PIN,SPI_FUNCTION); // SETEO EL PIN
		//SetPinsel( SSEL_PIN , PINSEL_GPIO );
		//SetPinMode( SSEL_PIN, PINMODE_PULLUP );

		Chip_IOCON_PinMux(LPC_IOCON,SSEL_PIN,PINMODE_PULLUP ,FUNC0);

		//SetPinDir(SSEL_PIN, PINDIR_OUTPUT);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,SSEL_PIN);  //led de alarma

		//SetPin( SSEL_PIN, HIGH ); //se pone en LOW para iniciar transferencia
		//Chip_GPIO_WritePortBit(LPC_GPIO,PORT_DISPLAY_0,PIN_DISPLAY_0,PASIVAR)
		Chip_GPIO_SetPinOutHigh(LPC_GPIO, SSEL_PIN);

		//SetPinsel( MISO_PIN , SPI_FUNCTION );
		Chip_IOCON_PinMuxSet(LPC_IOCON, MISO_PIN,SPI_FUNCTION);

		//SetPinsel( MOSI_PIN , SPI_FUNCTION );
		Chip_IOCON_PinMuxSet(LPC_IOCON, MOSI_PIN ,SPI_FUNCTION);

		PCONP |= (1<<8);	//alimentando periferico SPI

		PCLKSEL0 &= ~(3<<16);	//pongo 00 en SPI_CLCK
		PCLKSEL0 |= (SCK_CLCK<<16); //configuro Clock rate
		SPI->S0SPCCR = 8; //8: clock a 12,5MHz

		SPI->CPHA=0; //empieza transferencia cuando BUFFER!=0

		SPI->MSTR = SPI_MODE; //define modo
		SPI->BitEnable = 1;
		SPI->BITS = 0; //seteando 16 bits

		limpieza = SPI->S0SPSR;
		limpieza = SPI->S0SPDR;
		limpieza = limpieza;
}


static void taskMFRC522 (void)
{
	int16_t verificando_card;
	uint8_t i, noescliente;
	uint32_t aux;
	char tarjeta[15];
	tarjeta[0] = 0x0;
    uint64_t temp;


	while(1)
	{
		verificando_card =  Card(tarjeta);
	    //con el for se fija si es la tarjeta registrada
		if (verificando_card == 1)
		{
			verificando_card = -1;
			//Validamos clientes
		    for(i=0 ; i<CANT_CLIENTES ; i++)
		    {
				noescliente = 0;
				aux = (uint8_t) tarjeta[0];
				temp = (registros[i] & 0xff000000);
				noescliente = (temp == (aux * 1<<24)) ? noescliente : 1;
				aux = (uint8_t) tarjeta[1];
				temp = (registros[i] & 0x00ff0000);
				noescliente = (temp == (aux * 1<<16)) ? noescliente : 1;
				aux = (uint8_t) tarjeta[2];
				temp = (registros[i] & 0x0000ff00);
				noescliente = (temp == (aux * 1<<8)) ? noescliente : 1;
				aux = (uint8_t) tarjeta[3];
				temp = (registros[i] & 0x000000ff);
				noescliente = (temp == (aux * 1)) ? noescliente : 1;
				if(!noescliente)
				{
					verificando_card = i+1;
					i = CANT_CLIENTES;
				}
		    }
		}
		if (verificando_card == 0)
		{
			//xSemaphoreTake(xSemaforo,portMAX_DELAY);
			 //Chip_GPIO_WritePortBit(LPC_GPIO, 0, 22, 1);
			//xSemaphoreGive(xSemaforo);
		}
		else if (verificando_card < 0)
		{
			//Tarjeta sin registrar (-1)
		}
		else
		{
			//Tarjeta registrada (id=1, id=2, id=3
			xSemaphoreTake(xSemaforo,portMAX_DELAY);
			//Chip_GPIO_WritePortBit(LPC_GPIO, 0, 22, 0);
			//VA UN QUEUESEND () - ESTO LO VA A RECIBIR OTRA TAREA (WHILE(1) { QUEUERECEIVE()
			xSemaphoreGive(xSemaforo);
		}


	 vTaskDelay(100/portTICK_RATE_MS);

  }

}

int main (void)
{
	/*Inicializo el Clock del sistema*/
	Chip_SetupXtalClocking();
	SystemCoreClockUpdate();
	Board_Init();
	 SPI_init();//INICIALIZO LOS PINES

	 MFRC522_Init();

	 xSemaforo = xSemaphoreCreateMutex(); // mutex no pueden ser interrumpidos por otras tareas

		   xTaskCreate(taskMFRC522,"enviandoACK",1024,0,1,0);








	vTaskStartScheduler();

	while(1);

}



/*
===============================================================================
Led Blinky Oka
===============================================================================
*/
/*
#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <FreeRTOS.h>
#include <semphr.h>
#include <task.h>


xSemaphoreHandle xSemaforo; // debe ser global para que sea visto por todas las tareas

void TaskLedOff(void)
{
	while(1)
	{
		xSemaphoreTake(xSemaforo,portMAX_DELAY);
		Chip_GPIO_WritePortBit(LPC_GPIO, 0, 22, 0);
		vTaskDelay(1000/portTICK_RATE_MS);
		xSemaphoreGive(xSemaforo);
	}
}

void TaskLedOn(void)
{
	while(1)
	{
		xSemaphoreTake(xSemaforo,portMAX_DELAY);
		Chip_GPIO_WritePortBit(LPC_GPIO, 0, 22, 1);
		vTaskDelay(1000/portTICK_RATE_MS);
		xSemaphoreGive(xSemaforo);
	}
}
int main(void) {

    SystemCoreClockUpdate();
    Board_Init();

    xTaskCreate(TaskLedOff, "LedOff",1024,0,1,0);
    xTaskCreate(TaskLedOn, "LedOn",1024,0,1,0);

    xSemaforo = xSemaphoreCreateMutex(); // mutex no pueden ser interrumpidos por otras tareas
    vTaskStartScheduler();


    while(1);
}
*/
