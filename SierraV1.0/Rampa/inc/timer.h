
#ifndef TIMER_H_
#define TIMER_H_

#include "chip.h"

#define my_TIMER 	          LPC_TIMER0
#define my_TIMER_IRQ          TIMER0_IRQn
#define my_TIMER_SPEED 		  SYSCTL_PCLK_TIMER0
#define HANDLER_TIMER_NAME    TIMER0_IRQHandler

#define MATCH0 0
#define MATCH1 1
#define MATCH2 2
#define MATCH3 3

#define TIMER_TICK_RATE_HZ (200)

void inicializar_ADC(void);
void inicializar_GPIO(void);
void inicializar_DAC(void);
void init_timer(void);

#endif /* TIMER_H_ */
