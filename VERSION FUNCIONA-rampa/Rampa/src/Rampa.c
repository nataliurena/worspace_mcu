/*
===============================================================================
 Name        : Rampa.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here

#include <timer.h>
#include "chip.h"
#define UMBRAL 2048
#define LED_PORT 0
#define LED_PIN 22
#define activar_led		Chip_GPIO_SetPinOutHigh(LPC_GPIO, LED_PORT, LED_PIN)		//activa poniendo en 1 el led que indica que se esta por debajo del limite min configurado
#define pasivar_led		Chip_GPIO_SetPinOutLow(LPC_GPIO, LED_PORT, LED_PIN)			//pasiva poniendo en 0 el led que indica que se esta por debajo del limite min configurado
#define ADC_CH0_PORT 0
#define ADC_CH0_PIN 23
#define DAC_PORT 0
#define DAC_PIN 26

extern uint32_t iniciarADC_flag;
static uint16_t datoADC;

static ADC_CLOCK_SETUP_T ADCSetup;

// TODO: insert other definitions and declarations here

int main(void) {

#if defined (__USE_LPCOPEN)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
#if !defined(NO_BOARD_LIB)
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
    // Set the LED to the state of "On"
    Board_LED_Set(0, true);
#endif
#endif

    // TODO: insert code here

    SystemCoreClockUpdate();

    /* Inicializa el timer */
    inicializar_GPIO();
    init_timer();
    inicializar_DAC();
    inicializar_ADC();

    uint32_t i=0;

    // Enter an infinite loop, just incrementing a counter
    while(1)
    {

    	if(iniciarADC_flag)
		{
			iniciarADC_flag = 0;

			/* Reinicia la conversión */
			//Chip_ADC_SetStartMode(LPC_ADC, ADC_START_NOW, ADC_TRIGGERMODE_RISING);

			if (datoADC >= UMBRAL)
			{

				activar_led;
			}
			else
			{
				pasivar_led;
			}
		}
    	i++;

    }
    return 0 ;
}

/* Interrupción de ADC */
void ADC_IRQHandler(void)
{

	/* Lee el valor del ADC y lo guarda en dataADC */

	Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &datoADC);



	/* Reinicia la conversión */
	Chip_ADC_SetStartMode(LPC_ADC, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
}


/* Inicialización del ADC */
void inicializar_ADC(void)
{
	/* Asocia al pin a la función de ADC */
	Chip_IOCON_PinMux(LPC_IOCON,ADC_CH0_PORT,ADC_CH0_PIN,IOCON_MODE_INACT,IOCON_FUNC1);
	//Chip_IOCON_PinMux(LPC_IOCON,ADC_CH1_PORT,ADC_CH1_PIN,IOCON_MODE_INACT,IOCON_FUNC1);
	//Chip_IOCON_PinMux(LPC_IOCON,ADC_CH2_PORT,ADC_CH2_PIN,IOCON_MODE_INACT,IOCON_FUNC1);
	//Chip_IOCON_PinMux(LPC_IOCON,ADC_CH3_PORT,ADC_CH3_PIN,IOCON_MODE_INACT,IOCON_FUNC1);
	//Chip_IOCON_PinMux(LPC_IOCON,ADC_CH4_PORT,ADC_CH4_PIN,IOCON_MODE_INACT,IOCON_FUNC1);
	//Chip_IOCON_PinMux(LPC_IOCON,ADC_CH5_PORT,ADC_CH5_PIN,IOCON_MODE_INACT,IOCON_FUNC1);
	//Chip_IOCON_PinMux(LPC_IOCON,ADC_CH6_PORT,ADC_CH6_PIN,IOCON_MODE_INACT,IOCON_FUNC2);
	//Chip_IOCON_PinMux(LPC_IOCON,ADC_CH7_PORT,ADC_CH7_PIN,IOCON_MODE_INACT,IOCON_FUNC2);

	/* Inicializa el ADC */
	Chip_ADC_Init(LPC_ADC, &ADCSetup);
	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE);

	/* Velocidad de conversión */
	Chip_ADC_SetSampleRate(LPC_ADC, &ADCSetup, ADC_MAX_SAMPLE_RATE);

	/* Habilita la interrupción de ADC */
	NVIC_EnableIRQ(ADC_IRQn);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH0, ENABLE);

	/* Desactiva modo ráfaga */
	Chip_ADC_SetBurstCmd(LPC_ADC, DISABLE);

	/* Inicia la conversión */
	Chip_ADC_SetStartMode(LPC_ADC, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
}



void inicializar_DAC()
{
	/*Inicializo el DAC*/
	Chip_IOCON_PinMuxSet(LPC_IOCON,DAC_PORT,DAC_PIN,IOCON_FUNC2);
	Chip_DAC_Init(LPC_DAC);

}

void inicializar_GPIO()
{
//Salida para led
	Chip_IOCON_PinMuxSet(LPC_IOCON,LED_PORT,LED_PIN,FUNC0);	//configura el pin como funcion 0 porq se lo va a usar como entrada salida digital
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,LED_PORT,LED_PIN);  //direcciona el pin como salida, led indica fuera de rango por minimo

}




