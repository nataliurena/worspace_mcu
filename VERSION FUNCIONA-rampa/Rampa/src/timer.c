/*
 * timer.c
 *
 *  Created on: 15 ago. 2020
 *      Author: Hernan
 */
#include <timer.h>

uint32_t iniciarADC_flag = 0;

/* Interrupción de timer */
void HANDLER_TIMER_NAME(void)
{
	static uint16_t cuenta=0;
	if (Chip_TIMER_MatchPending(my_TIMER, MATCH0))
	{
		Chip_TIMER_ClearMatch(my_TIMER, MATCH0);

		Chip_DAC_UpdateValue(LPC_DAC,cuenta);
		//Chip_ADC_SetStartMode(LPC_ADC, ADC_START_NOW, ADC_TRIGGERMODE_RISING);

		iniciarADC_flag = 1;
		cuenta++;
		if(cuenta >= 1024)
			cuenta = 0;

	}
}

/* Inicialización de timer */
void init_timer(void)
{
	uint32_t TimerFreq;

	/* Habilita el timer elegido */
	Chip_TIMER_Init(my_TIMER);

	/* Levanta la frecuencia a la que corre el clock interno del timer */
    TimerFreq = Chip_Clock_GetPeripheralClockRate(my_TIMER_SPEED);

    /* Setea el timer en modo Match e Interrupción */
	Chip_TIMER_Reset(my_TIMER);
	Chip_TIMER_MatchEnableInt(my_TIMER, MATCH0);
	Chip_TIMER_SetMatch(my_TIMER, MATCH0, (TimerFreq / TIMER_TICK_RATE_HZ));

	/* Resetea en el match */
	Chip_TIMER_ResetOnMatchEnable(my_TIMER, MATCH0);

	/* Habilita la interrupción del timer */
	NVIC_EnableIRQ(my_TIMER_IRQ);
	NVIC_ClearPendingIRQ(my_TIMER_IRQ);

	/* Arranca el timer */
	Chip_TIMER_Enable(my_TIMER);
}
