/*
 * lcd.c
 *
 *  Created on: 22 sep. 2020
 *      Author: natal
 */


#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


#include "lcd.h"





void Lcd_Delay(int a)
{
    int j;
    int i;
    for(i=0;i<a;i++)
    {
        for(j=0;j<100;j++)
        {
        }
    }
}


void Lcd4_Port(char a)
{
	if(a & 1)
		D4_ON;
	else
		D4_OFF;

	if(a & 2)
		D5_ON;
	else
		D5_OFF;

	if(a & 4)
		D6_ON;
	else
		D6_OFF;

	if(a & 8)
		D7_ON;
	else
		D7_OFF;
}


void Lcd4_Cmd(char a)
{
	RS_OFF;             // => RS = 0
	Lcd4_Port(a);
	E_ON; // => E = 1
//	vTaskDelay(5);
   Lcd_Delay(5);
  E_OFF;             // => E = 0
}


void Lcd4_Clear(void)
{
	Lcd4_Cmd(0);
	Lcd4_Cmd(1);
}


void Lcd4_Set_Cursor(char a, char b)
{
	char temp,z,y;
	if(a == 1)
	{
	  temp = 0x80 + b;
		z = temp>>4;
		y = temp & 0x0F;
		Lcd4_Cmd(z);
		Lcd4_Cmd(y);
	}
	else if(a == 2)
	{
		temp = 0xC0 + b;
		z = temp>>4;
		y = temp & 0x0F;
		Lcd4_Cmd(z);
		Lcd4_Cmd(y);
	}
}

void Lcd4_Init(void)
{

	vTaskDelay(200);
	Lcd4_Cmd(0x02);
	Lcd4_Cmd(0x02);
	Lcd4_Cmd(0x08); //Lcd4_Cmd(0x0C);
	vTaskDelay(5);
	Lcd4_Cmd(0x00);
	Lcd4_Cmd(0x0C);
	vTaskDelay(5);
	Lcd4_Cmd(0x00);
	Lcd4_Cmd(0x01);
	vTaskDelay(10);
	Lcd4_Cmd(0x00);
	Lcd4_Cmd(0x04);



}


void Lcd4_Write_Char(char a)
{
   char temp,y;
   temp = a&0x0F;
   y = a&0xF0;
	 RS_ON;             // => RS = 1
   Lcd4_Port(y>>4);             //Data transfer
	 E_ON;

	 vTaskDelay(5);
	 //Lcd_Delay(5);
	 E_OFF;
	 Lcd4_Port(temp);
	 E_ON;
	 vTaskDelay(5);
	 //Lcd_Delay(5);
	 E_OFF;
}


void Lcd4_Write_String(char *a)
{
	int i;
	for(i=0;a[i]!='\0';i++)
	 Lcd4_Write_Char(a[i]);
}

void Lcd4_Shift_Right()
{
	Lcd4_Cmd(0x01);
	Lcd4_Cmd(0x0C);
}


void Lcd4_Shift_Left()
{
	Lcd4_Cmd(0x01);
	Lcd4_Cmd(0x08);
}


//INCILIZACION DE LOS PINES DEL LCD

LCD_Init(void)
{

	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_LCD_RS,PIN_LCD_RS,FUNC0); // P0.3 J22 SETEO EL PIN RS
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_LCD_E,PIN_LCD_E,FUNC0);//P0.21  J23
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_LCD_D4,PIN_LCD_D4,FUNC0);//P0.22 J24
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_LCD_D5,PIN_LCD_D5,FUNC0); // P0.27  J25 SETEO EL PIN D5
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_LCD_D6,PIN_LCD_D6,FUNC0);  //P0.28 J26
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_LCD_D7,PIN_LCD_D7,FUNC0); // P0.2 J21


	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_LCD_RS,PIN_LCD_RS); // rs J22  P0.3 J22
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_LCD_E,PIN_LCD_E); //   en P0.21  J23
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_LCD_D4,PIN_LCD_D4); // d4 P0.22 J24
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_LCD_D5,PIN_LCD_D5); // d5 P0.27  J25
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_LCD_D6,PIN_LCD_D6); // d6 P0.28 J26
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_LCD_D7,PIN_LCD_D7);  //d7 P0.2 J21


}


