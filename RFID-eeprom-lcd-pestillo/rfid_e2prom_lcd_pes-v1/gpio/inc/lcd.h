//LCD Functions Developed by electroSome

//LCD Module Connection
/*extern struct LCDport

extern bit RS;                                                                   
extern bit EN;                           
extern bit D0;
extern bit D1;
extern bit D2;
extern bit D3;
extern bit D4;
extern bit D5;
extern bit D6;
extern bit D7;
*/
#define PORT_LCD_RS  0
#define PIN_LCD_RS   3   //PO.3 J22 AZUL

#define PORT_LCD_E  0
#define PIN_LCD_E  21    //P0.21 J23 VIOLETA

#define PORT_LCD_D4  2
#define PIN_LCD_D4   13  // P2.13 J27 GRIS

#define PORT_LCD_D5  0
#define PIN_LCD_D5  27   //P0.27 J25 BLANCO

#define PORT_LCD_D6  0
#define PIN_LCD_D6  28    //P0.28 J26  NEGRO

#define PORT_LCD_D7  0
#define PIN_LCD_D7   2   //P0.2  J21 MARRON

#define RS_ON Chip_GPIO_SetPinOutHigh(LPC_GPIO,PORT_LCD_RS,PIN_LCD_RS)
#define RS_OFF Chip_GPIO_SetPinOutLow(LPC_GPIO,PORT_LCD_RS,PIN_LCD_RS)
#define E_ON Chip_GPIO_SetPinOutHigh(LPC_GPIO,PORT_LCD_E,PIN_LCD_E)
#define E_OFF Chip_GPIO_SetPinOutLow(LPC_GPIO,PORT_LCD_E,PIN_LCD_E)
#define D4_ON Chip_GPIO_SetPinOutHigh(LPC_GPIO,PORT_LCD_D4,PIN_LCD_D4)
#define D4_OFF Chip_GPIO_SetPinOutLow(LPC_GPIO,PORT_LCD_D4,PIN_LCD_D4)
#define D5_ON Chip_GPIO_SetPinOutHigh(LPC_GPIO,PORT_LCD_D5,PIN_LCD_D5)
#define D5_OFF Chip_GPIO_SetPinOutLow(LPC_GPIO,PORT_LCD_D5,PIN_LCD_D5)
#define D6_ON Chip_GPIO_SetPinOutHigh(LPC_GPIO,PORT_LCD_D6,PIN_LCD_D6)
#define D6_OFF Chip_GPIO_SetPinOutLow(LPC_GPIO,PORT_LCD_D6,PIN_LCD_D6)
#define D7_ON Chip_GPIO_SetPinOutHigh(LPC_GPIO,PORT_LCD_D7,PIN_LCD_D7)
#define D7_OFF Chip_GPIO_SetPinOutLow(LPC_GPIO,PORT_LCD_D7,PIN_LCD_D7)

//End LCD Module Connections 

void Lcd_Delay(int a);
void Lcd4_Port(char a);
void Lcd4_Cmd(char a);
void Lcd4_Clear(void);
void Lcd4_Set_Cursor(char a, char b);
void Lcd4_Init(void);
void Lcd4_Write_Char(char a);
void Lcd4_Write_String(char *a);
void Lcd4_Shift_Right(void);
void Lcd4_Shift_Left(void);
void LCD_Init(void);

