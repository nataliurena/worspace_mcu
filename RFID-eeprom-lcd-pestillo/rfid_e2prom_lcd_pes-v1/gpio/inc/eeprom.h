/*
 * eeprom.h
 *
 *  Created on: 20 sep. 2020
 *      Author: natal
 */

#ifndef EEPROM_H_
#define EEPROM_H_


#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif



/*
 * Hardware
 */
#define LED_PUERTO			(0)
#define LED_PIN				(22)
#define I2C_DEVICE			(I2C1)
#define I2C_DEVICE_NUM		(1)
#define I2C_DEVICE_PORT		(0)
#define I2C_SDA_PIN			(19)
#define I2C_SCL_PIN			(20)
#define I2C_SPEED			(100000)

/*
 * Otros
 */
#define FREQ_SISTEMA_HZ		(1000)
#define BUF_LEN				(64)

/*
 * Prototipos del sistema.
 */
void init_hardware(void);
uint32_t leer_24lc64(uint16_t address, int len, uint8_t *buf);
uint32_t escribir_24lc64(uint16_t address, uint32_t len, uint8_t *buf);







#endif /* EEPROM_H_ */
