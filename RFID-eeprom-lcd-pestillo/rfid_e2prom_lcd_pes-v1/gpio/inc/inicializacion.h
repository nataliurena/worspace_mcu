/*
 * inicializacion.h
 *
 *  Created on: 28 sep. 2020
 *      Author: natal
 */

#ifndef INICIALIZACION_H_
#define INICIALIZACION_H_




#define PESTILLO_PORT			  0
#define PESTILLO_PIN			  6   //J8


#define activar_pestillo			Chip_GPIO_SetPinOutHigh(LPC_GPIO,PESTILLO_PORT,PESTILLO_PIN)

#define pasivar_pestillo			Chip_GPIO_SetPinOutLow(LPC_GPIO,PESTILLO_PORT,PESTILLO_PIN)


void InicializarHardware(void);






#endif /* INICIALIZACION_H_ */
