/*
 * teclado.c
 *
 *  Created on: 1 oct. 2020
 *      Author: natal
 */


#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "string.h"
#include "teclado.h"



void InicializarHardwareTeclado(void)
{


	//salidas filas de teclado
    Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_FILA1,PIN_FILA1,FUNC0);
    Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_FILA2,PIN_FILA2,FUNC0);
    Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_FILA3,PIN_FILA3,FUNC0); //lpcopen
    Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_FILA4,PIN_FILA4,FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_FILA4,PIN_FILA4); // fila4   	j38
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_FILA3,PIN_FILA3); // fila3		j39
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_FILA2,PIN_FILA2); // fila2		j40
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,PORT_FILA1,PIN_FILA1);  //fila1		j41

	// entradas columnas de teclado
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_COLUMNA1,PIN_COLUMNA1,FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_COLUMNA2,PIN_COLUMNA2,FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_COLUMNA3,PIN_COLUMNA3,FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON,PORT_COLUMNA4,PIN_COLUMNA4,FUNC0);

	Chip_GPIO_SetPinDIRInput(LPC_GPIO,PORT_COLUMNA1,PIN_COLUMNA1);		//j47
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,PORT_COLUMNA2,PIN_COLUMNA2);		//j48
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,PORT_COLUMNA3,PIN_COLUMNA3);		//j49
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,PORT_COLUMNA4,PIN_COLUMNA4);		//j49

}



//FUNCIONES
char BarrerTeclado(void)
{

	int prueba[4];
    PasivarFila1;
    ActivarFila2;
    ActivarFila3;
    ActivarFila4;


	if (!(isActivoColumna1))			return '1';
	if (!(isActivoColumna2))			return '2';
	if (!(isActivoColumna3))			return '3';
	if (!(isActivoColumna4))			return 'A';

	ActivarFila1;
	PasivarFila2;
	ActivarFila3;
	ActivarFila4;

	if (!(isActivoColumna1))			return '4';
	if (!(isActivoColumna2))			return '5';
	if (!(isActivoColumna3))			return '6';
	if (!(isActivoColumna4))			return 'B';

	ActivarFila1;
	ActivarFila2;
	PasivarFila3;
	ActivarFila4;

	if (!(isActivoColumna1))			return '7';
	if (!(isActivoColumna2))			return '8';
	if (!(isActivoColumna3))			return '9';
	if (!(isActivoColumna4))			return 'C';

	ActivarFila1;
	ActivarFila2;
	ActivarFila3;
	PasivarFila4;


	if (!(isActivoColumna1))			return '*';
	if (!(isActivoColumna2))			return '0';
	if (!(isActivoColumna3))			return '#';
	if (!(isActivoColumna4))			return 'D';

	return BOTON_NO_PRESIONADO;

}






