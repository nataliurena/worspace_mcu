/*
 * sensorMag.c
 *
 *  Created on: 29 sep. 2020
 *      Author: natal
 */



#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "string.h"
#include "sensorMag.h"





void InitSmagnetico(void)
{

  Chip_IOCON_PinMuxSet(LPC_IOCON, Smag_PORT,Smag_PIN,FUNC0);//configura el pin como funcion 0 porq se lo va a usar como entrada salida digital
  Chip_GPIO_SetPinDIRInput(LPC_GPIO, Smag_PORT,Smag_PIN);//direcciona el pin como entrada pulsador_de_configuracion setea un maximo y minimo de medida


  Chip_IOCON_PinMuxSet(LPC_IOCON,BUZZ_PORT,BUZZ_PIN,FUNC0);	//configura el pin como funcion 0 porq se lo va a usar como entrada salida digital
  	Chip_GPIO_SetPinDIROutput(LPC_GPIO,BUZZ_PORT,BUZZ_PIN);  //direcciona el pin como salida,



  //Salida para led
  		Chip_IOCON_PinMuxSet(LPC_IOCON,0,22,FUNC0);	//configura el pin como funcion 0 porq se lo va a usar como entrada salida digital
  		Chip_GPIO_SetPinDIROutput(LPC_GPIO,0,22);  //direcciona el pin como salida, led indica fuera de rango por minimo




  	//entradas pulsadores de control
  		Chip_IOCON_PinMuxSet(LPC_IOCON,PULSADOR_magnetico_PORT	,PULSADOR_magnetico_PIN,FUNC0);			//configura el pin como funcion 0 porq se lo va a usar como entrada salida digital
  		Chip_GPIO_SetPinDIRInput(LPC_GPIO,PULSADOR_magnetico_PORT	,PULSADOR_magnetico_PIN);
}





