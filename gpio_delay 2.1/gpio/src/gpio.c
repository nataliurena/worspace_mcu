/*
===============================================================================
 Name        : gpio.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

xSemaphoreHandle xSemaforo; // Tiene que ser Global, porque lo tiene que poder ver todas las tareas


static taskLEDoff (void)
{


	while(1)
	{

	xSemaphoreTake(xSemaforo,portMAX_DELAY);
	Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
	vTaskDelay(1000 / portTICK_RATE_MS );  //ejemplo de delay, 1 seg apagado
	xSemaphoreGive(xSemaforo);
    }
	}



static taskLEDon (void)
{


	while(1)
		{

		xSemaphoreTake(xSemaforo,portMAX_DELAY);
		Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1);
        vTaskDelay(1000/portTICK_RATE_MS); //1 seg encendido

         xSemaphoreGive(xSemaforo);


		}
}









int main (void)
{
	SystemCoreClockUpdate();
	Board_Init();
	xTaskCreate(taskLEDoff,"taskLEDoff",1024,0,1,0);
	xTaskCreate(taskLEDon,"taskLEDon",1024,0,1,0);





	xSemaforo = xSemaphoreCreateMutex();


	vTaskStartScheduler();

	while(1);
}
