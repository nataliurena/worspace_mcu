/*
 * eeprom.c
 *
 *  Created on: 20 sep. 2020
 *      Author: natal
 */

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif




#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "eeprom.h"





void init_hardware(void)
{
	/*
	 * Inicializo el clock del sistema
	 */
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_100MHZ_CPU);
	SystemCoreClockUpdate();

	/*
	 * Inicializo los pines.
	 */
	Chip_GPIO_Init(LPC_GPIO);
	Chip_IOCON_Init(LPC_IOCON);
	Chip_GPIO_SetPinDIR(LPC_GPIO, LED_PUERTO, LED_PIN, true);
	Chip_GPIO_WritePortBit(LPC_GPIO, LED_PUERTO, LED_PIN, false);

	/*
	 * Inicializo el I2C.
	 */
	Chip_IOCON_PinMux(LPC_IOCON, I2C_DEVICE_PORT, I2C_SDA_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, I2C_DEVICE_PORT, I2C_SCL_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_EnableOD(LPC_IOCON,I2C_DEVICE_PORT, I2C_SDA_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON,I2C_DEVICE_PORT, I2C_SDA_PIN);
	Chip_I2C_Init(I2C_DEVICE_NUM);
	Chip_I2C_SetClockRate(I2C_DEVICE_NUM,I2C_SPEED);
	Chip_I2C_SetMasterEventHandler(I2C_DEVICE,Chip_I2C_EventHandlerPolling);

	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / FREQ_SISTEMA_HZ);
}

/*
 * Lee la memoria 24xx64
 * 		- address: 	La dirección de la memoria EEPROM a leer (de 0 a 8191).(0 a 0x1FFF)
 * 		- len:		La cantidad de bytes a leer de la memoria
 * 		- buf:		buffer donde pone los datos leidos.
 *
 * 		* devuelve la cantidad de datos que pudo leer.
 */
uint32_t leer_24lc64(uint16_t address, int len, uint8_t *buf)
{
	I2C_XFER_T xfer = {0};
	uint8_t cmd[2];
	cmd[0] = (uint8_t)((address>>8)&0x1F);// 0x1FFF 1 1111 1111 11111
	cmd[1] = (uint8_t)(address&0xFF);
	xfer.slaveAddr = 0x50;
	xfer.txBuff = cmd;
	xfer.txSz = 2;
	xfer.rxBuff = buf;
	xfer.rxSz = len;
	while (Chip_I2C_MasterTransfer(I2C_DEVICE, &xfer) == I2C_STATUS_ARBLOST) {}
	return len - xfer.rxSz;

}

/*
 * Escribe la memoria 24LC64.
 * 		- address: 	La dirección de la memoria EEPROM a partir de la cual se
 * 					escriben los datos (de 0 a 8191).
 * 		- len:		La cantidad de datos a escribir (entre 0 y 32) ya que
 * 					no se pueden escribir más de 32 datos en una única
 * 					operación. http://ww1.microchip.com/downloads/en/devicedoc/21189f.pdf
 * 		- buf:		El buffer con los datos a escribir.
 *
 * 		* devuelve la cantidad de datos que no pudo escribir (0 si está todo bien)
 */
uint32_t escribir_24lc64(uint16_t address, uint32_t len, uint8_t *buf)
{
	I2C_XFER_T xfer = {0};
	uint8_t cmd[34];
	cmd[0] = (uint8_t)((address>>8)&0x1F);
	cmd[1] = (uint8_t)(address&0xFF);
	if(len>32) return -1;
	memcpy(&cmd[2],buf,len);
	xfer.slaveAddr = 0x50;
	xfer.txBuff = cmd;
	xfer.txSz = len+2;
	xfer.rxBuff = NULL;
	xfer.rxSz = 0;
	while (Chip_I2C_MasterTransfer(I2C_DEVICE, &xfer) == I2C_STATUS_ARBLOST) {}
	return xfer.txSz;
}






