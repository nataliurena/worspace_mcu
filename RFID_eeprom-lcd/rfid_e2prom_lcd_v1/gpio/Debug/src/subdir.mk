################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Rfid.c \
../src/cr_startup_lpc175x_6x.c \
../src/crp.c \
../src/eeprom.c \
../src/gpio.c \
../src/lcd.c \
../src/sysinit.c \
../src/timer.c 

OBJS += \
./src/Rfid.o \
./src/cr_startup_lpc175x_6x.o \
./src/crp.o \
./src/eeprom.o \
./src/gpio.o \
./src/lcd.o \
./src/sysinit.o \
./src/timer.o 

C_DEPS += \
./src/Rfid.d \
./src/cr_startup_lpc175x_6x.d \
./src/crp.d \
./src/eeprom.d \
./src/gpio.d \
./src/lcd.d \
./src/sysinit.d \
./src/timer.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_LPCOPEN -D__LPC17XX__ -D__REDLIB__ -I"C:\Users\natal\Desktop\Proyecto TDIII\RFID_eeprom-lcd\rfid_e2prom_lcd_v1\gpio\inc" -I"C:\Users\natal\Desktop\Proyecto TDIII\RFID_eeprom-lcd\rfid_e2prom_lcd_v1\lpc_board_nxp_lpcxpresso_1769\inc" -I"C:\Users\natal\Desktop\Proyecto TDIII\RFID_eeprom-lcd\rfid_e2prom_lcd_v1\lpc_chip_175x_6x\inc" -I"C:\Users\natal\Desktop\Proyecto TDIII\RFID_eeprom-lcd\rfid_e2prom_lcd_v1\freertos\inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


