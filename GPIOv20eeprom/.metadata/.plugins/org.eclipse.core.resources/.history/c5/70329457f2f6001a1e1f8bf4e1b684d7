/*
 * Rfid.h
 *
 *  Created on: 11 sep. 2020
 *      Author: natal
 */

#ifndef RFID_H_
#define RFID_H_


#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif


//!< ///////////////////   PCONP   //////////////////////////
	//!<  Power Control for Peripherals register (PCONP - 0x400F C0C4) [pag. 62 user manual LPC1769]
	//!< 0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
	#define 	PCONP	(* ( ( __RW uint32_t  * ) 0x400FC0C4UL ))


//!< ///////////////////   PCLKSEL   //////////////////////////
	//!< Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 -0x400F C1A8 and PCLKSEL1 - 0x400F C1AC) [pag. 56 user manual]
	//!< 0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los CLKs de los dispositivos:
	#define		PCLKSEL		( ( __RW uint32_t  * ) 0x400FC1A8UL )
	//!< Registros PCLKSEL
	#define		PCLKSEL0	PCLKSEL[0]
	#define		PCLKSEL1	PCLKSEL[1]



//------------------MFRC522 Register---------------
//Page 0:Command and Status
#define     Reserved00            0x00
#define     CommandReg            0x01
#define     CommIEnReg            0x02
#define     DivlEnReg             0x03
#define     CommIrqReg            0x04
#define     DivIrqReg             0x05
#define     ErrorReg              0x06
#define     Status1Reg            0x07
#define     Status2Reg            0x08
#define     FIFODataReg           0x09
#define     FIFOLevelReg          0x0A
#define     WaterLevelReg         0x0B
#define     ControlReg            0x0C
#define     BitFramingReg         0x0D
#define     CollReg               0x0E
#define     Reserved01            0x0F

//Page 1:Command
#define     Reserved10            0x10
#define     ModeReg               0x11
#define     TxModeReg             0x12
#define     RxModeReg             0x13
#define     TxControlReg          0x14
#define     TxAutoReg             0x15
#define     TxSelReg              0x16
#define     RxSelReg              0x17
#define     RxThresholdReg        0x18
#define     DemodReg              0x19
#define     Reserved11            0x1A
#define     Reserved12            0x1B
#define     MifareReg             0x1C
#define     Reserved13            0x1D
#define     Reserved14            0x1E
#define     SerialSpeedReg        0x1F
//Page 2:CFG
#define     Reserved20            0x20
#define     CRCResultRegM         0x21
#define     CRCResultRegL         0x22
#define     Reserved21            0x23
#define     ModWidthReg           0x24
#define     Reserved22            0x25
#define     RFCfgReg              0x26
#define     GsNReg                0x27
#define     CWGsPReg              0x28
#define     ModGsPReg             0x29
#define     TModeReg              0x2A
#define     TPrescalerReg         0x2B
#define     TReloadRegH           0x2C
#define     TReloadRegL           0x2D
#define     TCounterValueRegH     0x2E
#define     TCounterValueRegL     0x2F
//Page 3:TestRegister
#define     Reserved30            0x30
#define     TestSel1Reg           0x31
#define     TestSel2Reg           0x32
#define     TestPinEnReg          0x33
#define     TestPinValueReg       0x34
#define     TestBusReg            0x35
#define     AutoTestReg           0x36
#define     VersionReg            0x37
#define     AnalogTestReg         0x38
#define     TestDAC1Reg           0x39
#define     TestDAC2Reg           0x3A
#define     TestADCReg            0x3B
#define     Reserved31            0x3C
#define     Reserved32            0x3D
#define     Reserved33            0x3E
#define     Reserved34            0x3F

// Mifare_One card command word
# define PICC_REQIDL          0x26               // find the antenna area does not enter hibernation
# define PICC_REQALL          0x52               // find all the cards antenna area
# define PICC_ANTICOLL        0x93               // anti-collision
# define PICC_SElECTTAG       0x93               // election card
# define PICC_AUTHENT1A       0x60               // authentication key A
# define PICC_AUTHENT1B       0x61               // authentication key B
# define PICC_READ            0x30               // Read Block
# define PICC_WRITE           0xA0               // write block
# define PICC_DECREMENT       0xC0               // debit
# define PICC_INCREMENT       0xC1               // recharge
# define PICC_RESTORE         0xC2               // transfer block data to the buffer
# define PICC_TRANSFER        0xB0               // save the data in the buffer
# define PICC_HALT            0x50               // Sleep




//MF522 Command word
#define PCD_IDLE              0x00               //NO action; Cancel the current command
#define PCD_AUTHENT           0x0E               //Authentication Key
#define PCD_RECEIVE           0x08               //Receive Data
#define PCD_TRANSMIT          0x04               //Transmit data
#define PCD_TRANSCEIVE        0x0C               //Transmit and receive data,
#define PCD_RESETPHASE        0x0F               //Reset
#define PCD_CALCCRC           0x03               //CRC Calculate





#define SPI ((__RW spi_t *) 0x40020000UL)



#define     __R				volatile const  	// !< Modificador para solo lectura
#define 	__W     		volatile 	       	// !<  Modificador para solo escritura
#define 	__RW			volatile           	// !< Modificador lectura / escritura

typedef struct{//MAPA DE REGISTROS DE SPI

	union{
		__W uint32_t S0SPCR; //SPI CONTROL REGISTER (setea SPI0 con los siguientes parametros):
		struct{
			__RW uint32_t RESERVED_1:2;
			__RW uint32_t BitEnable:1;//0: send and receives 8 bits (DATALOW); 1: amplia BUFFER sumandole BITS(BITS=nombre de registro)
			__RW uint32_t CPHA:1;//0:muestra en primer flanco de SCK. Empieza cuando se activa SSEL signal; 1:
			__RW uint32_t CPOL:1;//Polarizacion de pulso SCK(tiempo). 0:SCK active high; 1:SCK active low
			__RW uint32_t MSTR:1;//Seleccion de Modo. 0: slave mode; 1:Master mode
			__RW uint32_t LSBF:1;//Sea BUFFER byte de transferencia. 0:MSB(lee BUFFER de izquierda a derecha); 1:LSB (derecha a izquierda)
			__RW uint32_t SPIE:1;//Habilitacion de INTERRUPCION. 0:interrupt inhibited; 1: interrupcion generada cada vez que SPIF o MODF se activen
			__RW uint32_t BITS:4;//si BitEnable=1 -> BITS=number of bits per transfer (Ej: 1000 = 8 bits per transfer. Ej2: 1100 = 12 bits per transfer. Ej3: 0000 = 16 bits per transfer)
			__RW uint32_t RESERVED_2:20;
		};
	};

	union{
		__RW uint32_t S0SPSR; //SPI STATUS REGISTER (los bits de este registro se BORRAN/LIMPIAN al ser leidos):
		struct{
			__RW uint32_t RESERVED_3:3;
			__RW uint32_t ABRT:1; //Se pone en 1 si el Slave se desconecta.
			__RW uint32_t MODF:1;//Se pone en 1 indicando falla de Modo(error set control).
			__RW uint32_t ROVR:1;//Se pone en 1 cuando read overrun (BUFFER lectura lleno)
			__RW uint32_t WCOL:1;//Se pone en 1 cuando write collision (BUFFER salida lleno)
			__RW uint32_t SPIF:1;//TRANSFER COMPLETE (flag) -> se pone 1. Ocurre en el ultimo ciclo de transferencia
			__RW uint32_t RESERVED_4:24;
		};
	};

	union{
		__RW uint32_t S0SPDR;//SPI BI-DIRECTIONAL DATA REGISTER(entradas y salidas):
//NOTAS DE DATA REGISTER: cuando se usa como MASTER, escribiendo en este registro se empieza el SPI data transfer.
//		cuando se empieza la transferencia se bloquea este buffer y no se puede escribir. Lo mismo ocurre con SPIF=1 (leyendo SPIF se limpia)
		struct{
			__RW uint32_t BUFFER:8;//puerto de SPI bi-directional
			__RW uint32_t DataHigh:8;//si BitEnable=1 -> se habilita este espacio para ampliar el BUFFER sumandole BITS(nombre de registro)
			__RW uint32_t RESERVED_5:16;
			};
		};

	union{
		__RW uint32_t S0SPCCR;//CONTROLS FREQUENCY OF MASTER's SCK0(frecuencia de pulsos de tiempo):
		struct{
			__RW uint32_t CLOCK:8;//setea SCK(numero de pulsos por ciclo). Si es Master-> T>=8. Si es Slave-> T< 1/8 de SPI of Master
			__RW uint32_t RESERVED_6:24;
			};
		};

//	union{
//		__RW uint32_t S0SPINT;//SPI INTERRUPT FLAG
//		struct{
//			__RW uint32_t SPINT:1;//se borra con un 1
//		};
//		__RW uint32_t RESERVED_7:31;
//	};
//
}spi_t;



//And MF522 The error code is returned when communication
#define MI_OK                 0
#define MI_NOTAGERR           1
#define MI_ERR                2



#define SCK_CLCK 0b01	//0b00= CCLK/4		; 	0b01= CCLK	; 	0b10= CCLCK/2	;	 0b11= CCLCK/8
#define SPI_MODE 1//Modo que va a tomar el micro: 0: Slave ; 	1: Master
#define SPI_FUNCTION 3


//----------PINES----------------------------------------------------------------------------------------
#define SCK_PIN		0, 15 //alternativa en    0, 7  1, 20
#define SSEL_PIN	0, 16//alternativa en     0, 6   1, 21
#define MISO_PIN	 0, 17//alternativa en    0, 8  1, 23
#define MOSI_PIN	0, 18 //alternativa en	  0, 9   1, 24



#define LOW		0 //bajo
#define HIGH	1 //alto




#endif /* RFID_H_ */
